#include "stm32f0xx.h"
#include "user_functions.h"
#include "flash.h"
#include "board_V02.h"
#include "motor_driver.h"


// TODO:
/*
	- overcurrent monitoring, not just fixed value?
	- longer + shorter cycle time (% of semi time?)
	- maybe add two-shot bursts? and four?
	
	- test longer and shorter time, is 5% ok?
*/


volatile int sleep_timer = 0;


// EXTI handler, waking up controller
void EXTI4_15_IRQHandler(void)
{
	EXTI_ClearITPendingBit(EXTI_Line10);
	sleep_timer = 0;
}



// MAIN ROUTINE
int main(void)
{	
	Config_t Config;
	FireCode_t code;
	int voltage;
	volatile int timeout;
	
	// init board
	RCC_DeInit();
	Board_Init();
	
	timeout = 100000;
	while (timeout--);
	
	// init rest
	Delay_Init();
	TIM14_Init();
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);
	SysTick_Config(1000*8); // tick every ms
	
	// intro
	Delay_ms(1000);
	
	// check battery, error if outside of safe region
	voltage = GetBatteryVoltage();
	if (voltage < 6500 || voltage > 16500)
	{
		MOTOR_LongBuzz(3);
		while (1);
	}
	
	// read config
	CONFIG_Read(&Config);
	
	// welcome buzz
	MOTOR_LongBuzz(1);
	
	// if uncalibrated, buzz twice
	if (Config.learned == 0)
		MOTOR_LongBuzz(1);
	
	// wait 3 seconds
	Delay_ms(3000);	
	
	// programming mode
	if (TriggerPressed())
	{
		// confirm we are in programming mode
		MOTOR_BeepUp();
		
		// wait for trigger release
		while (TriggerPressed());
		
		// enter programming mode
		MENU_ProgrammingMode(&Config);
		
		// confirm exit of programming mode
		Delay_ms(500);
		MOTOR_BeepDown();
		Delay_ms(500);
	}
	
	// another buzz
	MOTOR_Buzz(1);
	
	// MAIN LOOP
	while (1)
	{
		if (TriggerPressed())
		{
			// run main driver
			code = MOTOR_MainDriver(&Config);
			
			// warning after some codes
			switch (code)
			{
				case Fire_OverT:
					MOTOR_LongBuzz(3);
					Delay_ms(5000);
					break;
				
				case Fire_UnderV:
					MOTOR_LongBuzz(2);
					break;
					
				case Fire_Learned:
					MOTOR_LongBuzz(1);
					break;
				
				default: break;
			}
		}
		else
		{
			// sleep after 10 seconds of inactivity
			if (sleep_timer > SLEEP_TIMEOUT)
				PWR_EnterSTOPMode(PWR_Regulator_ON, PWR_STOPEntry_WFI);
		}
		
		// delay
		Delay_ms(1);
		
		// sleep timer
		if (sleep_timer < SLEEP_TIMEOUT+10)
			sleep_timer++;
	}
}
