/*****************************************/ 
/**	Tomaz Percic 2016										**/
/**																			**/
/**	Library for writing and reading			**/
/**	from/to flash												**/
/*****************************************/

#ifndef	_FLASH_H
#define _FLASH_H

#include "stm32f0xx.h"
#include "string.h"

#define PAGE15_ADDR		0x08003C00 // page 15 start
#define PAGE31_ADDR		0x08007C00 // page 31 start
#define PAGE127_ADDR	0x0803F800 // page 127 start
#define PAGE_SIZE			0x0400

// function prototypes
uint8_t T_FLASH_ErasePage(uint32_t page_address);
uint8_t T_FLASH_Write(uint32_t page_address, uint8_t* data, uint32_t size);
void T_FLASH_Read(uint32_t page_address, uint8_t* data, uint16_t size);

#endif
