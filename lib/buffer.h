/**	Tomaz Percic 2016										**/
/**	Library for buffer functions				**/

#ifndef BUFFER_H
#define BUFFER_H

#include "string.h"
#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"


// struct definition
typedef struct
{
	unsigned int size;
	uint8_t *data;
	unsigned int read;
	unsigned int write;
}
buffer_struct_t;

// function prototypes
void Buffer_Init(buffer_struct_t *structure, int size);
void Buffer_Reset(buffer_struct_t *structure);
int Buffer_Write(buffer_struct_t *structure, uint8_t *data, int size);
int Buffer_Write_Simple(buffer_struct_t *structure, uint8_t *data, int size);
int Buffer_Read(buffer_struct_t *structure, uint8_t *data, int size);
unsigned int Buffer_RemainingSpace(buffer_struct_t *structure);
//unsigned int Buffer_Empty(buffer_struct_t *structure);


#endif
