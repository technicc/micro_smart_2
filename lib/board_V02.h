/**	Tomaz Percic 2017										**/
/**	Library for board definitions				**/

#ifndef _BOARD_V01_H
#define _BOARD_V01_H

#include "stm32f0xx.h"
#include "user_functions.h"

// pin definitions
#define MSENSE_PIN								GPIO_Pin_1 //PB
#define MSENSE_ADC_CHANNEL				ADC_Channel_9
#define MSENSE_ADC_SAMPLE_TIME		ADC_SampleTime_239_5Cycles

 //ADC_SampleTime_71_5Cycles
 //ADC_SampleTime_239_5Cycles

#define HIN_PIN										GPIO_Pin_6 //PA
#define HIN_PINSRC								GPIO_PinSource6

#define LIN_PIN										GPIO_Pin_7 //PA
#define LIN_PINSRC								GPIO_PinSource7

#define TRIG_PIN									GPIO_Pin_10 //PA

#define BATT_PIN									GPIO_Pin_5 //PA
#define BATT_ADC_CHANNEL					ADC_Channel_5
#define BATT_ADC_SAMPLE_TIME			ADC_SampleTime_71_5Cycles

// other defines
#define MOTOR_PWM_LEVELS					100
#define MOTOR_PWM_FREQ						300


// function prototypes
void Board_Init(void);

void MOTOR_On(void);
void MOTOR_Off(void);
void MOTOR_Buzz(int n_buzzes);
void MOTOR_LongBuzz(int n_buzzes);
void MOTOR_Play(void);
void MOTOR_BeepUp(void);
void MOTOR_BeepDown(void);

void MOTOR_SetBrakePWM(uint8_t pwm);

uint16_t MOTOR_GetCurrent(void);

uint8_t TriggerPressed(void);
uint16_t GetBatteryVoltage(void);
float GetChipTemperature(void);

#endif
