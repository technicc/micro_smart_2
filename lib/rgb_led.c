/**	Tomaz Percic 2017										**/
/**	Library for RGB LED on mosfet				**/

#include "rgb_led.h"


// global variables
uint8_t red = 0, green = 0, blue = 0;


// RGB LED driver
void LED_Driver(void)
{
	static uint8_t counter = 0;
	
	// turn on
	if (counter == 0)
	{
		if (red > 0)
			R_LED_ON;
		
		if (green > 0)
			G_LED_ON;
		
		if (blue > 0)
			B_LED_ON;
	}
	
	// red
	if (counter == red)
		R_LED_OFF;
	
	// green
	if (counter == green)
		G_LED_OFF;
	
	// blue
	if (counter == blue)
		B_LED_OFF;
	
	// increase counter
	counter++;
	if (counter == 10)
		counter = 0;
}


// set RGB values
void LED_SetRGB(uint8_t red_val, uint8_t green_val, uint8_t blue_val)
{
	red = red_val;
	green = green_val;
	blue = blue_val;
}


// display value with LEDs
void LED_DisplayValue(uint8_t percent)
{
	// set white
	red = 10;
	green = 10;
	blue = 10;
	
	// limit
	if (percent > 100)
		percent = 100;
	
	// set colors
	if (percent < 25)
	{
		// red = 0, green = 0 to 1, blue = 1
		red = 0;
		green = 4 * percent / 10;
	}
	else if (percent < 50)
	{
		// red = 0, green = 1, blue = 1 to 0
		red = 0;
		blue = 10 - (4*(percent - 25) / 10);
	}
	else if (percent < 75)
	{
		// red = 0 to 1, green = 1, blue = 0
		red = 4 * (percent - 50) / 10;
		blue = 0;
	}
	else
	{
		// red = 1, green = 1 to 0, blue = 0
		green = 10 - (4*(percent - 75) / 10);
		blue = 0;
	}
}


// show LED intro
void LED_Intro(void)
{
	red = 0;
	green = 0;
	blue = 0;
	
	//R	xxx___xxx___
	//G	_xxx___xxx__
	//B	__xxx___xxx_
	
	red = 10;
	Delay_ms(200);
	red = 0;
	
	green = 10;
	Delay_ms(200);
	green = 0;
	
	blue = 10;
	Delay_ms(200);
	blue = 0;
}


// turn off led
void LED_Off(void)
{
	red = 0;
	green = 0;
	blue = 0;
}
