/*****************************************/ 
/**	Tomaz Percic 2015										**/
/**																			**/
/**	Library for user functions for the	**/
/**	F0xx chip														**/
/*****************************************/

#ifndef USER_FUNCTIONS_H
#define USER_FUNCTIONS_H

#include "stm32f0xx.h"

void Init_SetPLLHSIClock(uint32_t pll_mult);

void Delay_Init(void);
void Delay_ms(int val);
void Delay_us(int val);

void TIM14_Init(void);

#define INIT_TIME							(TIM14_Init())
#define TIME_FROM_HERE				(TIM14->CNT = 0)
#define TIME_TO_HERE					{timer=TIM14->CNT; if(timer>max) max=timer; sprintf(buffer, "Timer: %4d\n", max); T_USART_SendString(USART1, buffer);}

#endif
