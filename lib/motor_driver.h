/**	Tomaz Percic 2017										**/
/**	Library for motor driver						**/

#ifndef _MOTOR_DRIVER_H
#define _MOTOR_DRIVER_H

#include "stm32f0xx.h"
#include "board_v02.h"
#include "flash.h"

// defines
#define N_FILT			4		
#define N_FILT2			64

#define TRIG_PRESS_TIMEOUT			200 // x10 = ms
#define TRIG_HELD_TIMEOUT1			300 // x10 = ms
#define TRIG_HELD_TIMEOUT2			600 // x10 = ms
#define TRIG_HELD_TIMEOUT3			1000 // x10 = ms

#define SEL_SEMI_AUTO			1
#define SEL_SEMI_BURST		2
#define SEL_SEMI_BURST_H	3
#define SEL_SEMI_SEMI			4

#define BAT_LIPO_7_4			1
#define	BAT_LIPO_11_1			2
#define BAT_LIPO_14_8			3
#define BAT_LIFE_9_6			4
#define BAT_LIFE_12_8			5

#define	SLEEP_TIMEOUT			1800000	// in miliseconds, 3600000 is 1 hour, 1800000 is 30 min

// motor driver struct
typedef struct
{
	uint16_t current;
	uint16_t voltage;
	uint16_t rpm;
	uint16_t max_current;
	uint16_t Rint;
	int cycles;
}
MotorDriver_t;


// configuration struct
typedef struct
{
	uint8_t 	battery_type;
	uint8_t 	fire_selector;
	int 			semi_time;
	int				burst_time;
	uint8_t		precocking;
	uint8_t 	drive_pwm;
	uint8_t 	brake_pwm;
	uint16_t	burst_to_auto;
	uint16_t 	semi_delay;
	
	uint8_t 	learned;
	uint16_t	learn_voltage;
	
	int16_t 	max_temperature;
	int16_t 	min_temperature;
	
	uint8_t 	virgin_marker;
} Config_t;


typedef enum
{
	Fire_OK = 0x00,
	Fire_Learned,
	Fire_OverT,
	Fire_OverI,
	Fire_UnderV
} FireCode_t;


// function prototypes
FireCode_t MOTOR_MainDriver(Config_t* Config);

void CONFIG_Store(Config_t* Config);
void CONFIG_Read(Config_t* Config);
void CONFIG_Reset(Config_t* Config);

void MENU_ProgrammingMode(Config_t* Config);

uint8_t GetBatteryLevel(Config_t* Config, int voltage);
uint8_t CountTriggerPresses(void);
uint8_t BrakingDone(void);

#endif
