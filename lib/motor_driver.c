/**	Tomaz Percic 2017										**/
/**	Library for motor driver						**/

#include "motor_driver.h"


// GLOBAL VARIABLES
uint8_t hybrid_auto = 0;
uint8_t cycles = 0;
int one_cycle = 0;

// GLOBAL FLAGS
uint8_t over_current = 0;
uint8_t under_voltage = 0;
uint8_t over_temp = 0;

// SYSTICK FLAGS
volatile uint8_t brake = 0;


/**
  * @brief  SysTick_Handler handles motor braking time
  * @note   SysTick_Handler is called once every 1 ms
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
	static uint8_t brake_old = 0;
	static uint32_t counter = 0;
	
	// brake request?
	if (brake_old == 0 && brake == 1)
		counter = 0;
	
	// brake timeout?
	if (brake == 1)
	{
		counter++;
		
		// after 200 ms, turn off braking
		if (counter > 200)
			MOTOR_SetBrakePWM(0);
		
		// after 10 ms more, reset flag
		if (counter > 210)
		{
			brake = 0;
			counter = 0;
		}
	}
	
	// store previous value
	brake_old = brake;
}


/**
  * @brief  This is the main shot driver function
  * @note   The function handles all firing modes, checks for over-current and over-temperature, etc.
  * @param  Config: config struct, which stores all parameters for the gun
  * @retval FireCode_t: post fire code (OK, overT, overI, ...)
  */
FireCode_t MOTOR_MainDriver(Config_t* Config)
{
	int correction, semi_time, burst_time, precock_time, held_time;
	int cnt = 0;
	int pwm_comp;
	uint8_t pwm_on = 0;
	int voltage;
	int current = 0;
	uint8_t SoC;
	uint8_t motor_on = 0;
	
	// turn of braking if not yet done
	if (brake)
	{
		MOTOR_SetBrakePWM(0);
		Delay_ms(5);
		brake = 0;
	}
	
	// check error flags
	if (over_current)
		return Fire_OverI;
	if (over_temp)
		return Fire_OverT;
	if (under_voltage)
		return Fire_UnderV;
	
	// check battery voltage and calibrate semi/burst cycle time
	voltage = GetBatteryVoltage();
	if (voltage == 0)
		return Fire_UnderV; // avoid dividing with 0
	
	correction = 128 * Config->learn_voltage / voltage;
	semi_time = correction * Config->semi_time / 128;
	burst_time = correction * Config->burst_time / 128;
	
	// turn on motor	
	MOTOR_On();
	motor_on = 1;
	
	// start loop
	TIM14->CNT = 0;
	cnt = 0;
	while (1)
	{
		// get current
		if (motor_on == 1)
			current = MOTOR_GetCurrent();
		
		// monitor current
		if (current > 300)
		{
			MOTOR_Off(); // if above 300A, err, exit
			motor_on = 0;
			over_current = 1;
			return Fire_OverI;
		}
		
		// electronic fuse
		
		
		// PWM driving
		if (pwm_on == 1)
		{
			pwm_comp = cnt % 100; //pwm_comp is 0 to 99
		
			if (pwm_comp < Config->drive_pwm)
			{
				MOTOR_On();
				motor_on = 1;
			}
			else if (pwm_comp >= Config->drive_pwm)
			{
				MOTOR_Off();
				motor_on = 0;
			}
		}
		
		// break on trigger release
		if (TriggerPressed() == 0)
		{
			hybrid_auto = 0;
			pwm_on = 0;
			break;
		}
		
		// functions only if learned
		if (Config->learned == 1)
		{
			// PWM driving (after one semi shot)
			if (cnt > semi_time)
				pwm_on = 1;
			
			// SEMI only mode, break on one cycle
			if (Config->fire_selector == SEL_SEMI_SEMI)
			{
				if (cnt > semi_time)
					break;
			}
			
			// BURST mode, break on three cycles
			if (Config->fire_selector == SEL_SEMI_BURST)
			{
				if (cnt > burst_time)
					break;
			}
			
			// BURST HYBRID mode, first time break on burst, if held, don't break on burst
			if (Config->fire_selector == SEL_SEMI_BURST_H && hybrid_auto == 0)
			{
				if (cnt > burst_time)
					break;
			}
		}
		
		// increase cnt
		cnt = TIM14->CNT;
	}
	
	// PRECOCKING, keep motor running for precocking time
	if (Config->learned == 1 && Config->precocking > 0 && Config->fire_selector == SEL_SEMI_SEMI)
	{
		// calculate time
		precock_time = Config->precocking * semi_time / 100;
		
		// start loop
		cnt = 0;
		TIM14->CNT = 0;
		while (1)
		{
			// break on precocking time
			if (cnt > precock_time)
				break;
			
			cnt = TIM14->CNT;
		}
	}
	
	// turn on braking
	MOTOR_Off();
	Delay_us(10);
	MOTOR_SetBrakePWM(Config->brake_pwm);
	brake = 1;
	
	// SEMI DELAY
	if (Config->learned == 1)
		Delay_ms(Config->semi_delay);

	// BURST HYBRID, wait for trigger release and count
	held_time = 0;
	while (TriggerPressed())
	{
		// if trigger held more than "burst to auto delay", break loop
		if (Config->fire_selector == SEL_SEMI_BURST_H  &&  held_time > Config->burst_to_auto)
		{
			hybrid_auto = 1;
			break;
		}

		// wait
		held_time++;
		Delay_ms(1);
	}
	
	// check over-temperature
	if (GetChipTemperature() > 70)
	{
		while (brake); // wait to finish braking
		return Fire_OverT;
	}
	
	// check battery SoC
	SoC = GetBatteryLevel(Config, GetBatteryVoltage());
	if (SoC < 15 && SoC > 5)
	{
		// warning
		while (brake); // wait to finish braking
		return Fire_UnderV;
	}
	else if (SoC <= 5)
	{
		// disable firing
		while (brake); // wait to finish braking
		under_voltage = 1;
		return Fire_UnderV;
	}
	
	// LEARN MODE
	if (Config->learned == 0)
	{
		cycles++;
		
		if (cycles > 1)
		{
			one_cycle += cnt;
			
			if (cycles == 6)
			{
				// calculate and save times
				Config->semi_time = 104 * one_cycle / 512; // a bit more than fifth of cycle
				Config->burst_time = 308 * one_cycle / 512; // exactly three cycles
				Config->learned = 1;
				
				// also save voltage at which the learning was done
				Delay_ms(200);
				while (brake);
				Config->learn_voltage = GetBatteryVoltage();
				CONFIG_Store(Config);
				
				// confirm learned
				return Fire_Learned;
			}
		}
	}
	
	/* zakaj je while(brake); problem?
	- za zacetek, optimizacija je problem
	- ce spremenljivka NI volatile, potem compiler predvideva, da je nemore nobeden drug spremenit
	- zato je samo pred while stavkom vrednost nalozu in kasneje ni bilo vec Load ukaza.
	- kadar je volatile zraven, je vedno load ukaz 
	- lahk si vidu prej v dissasembly, da je bil R0 register vedno 1, ker je bila to vrednost `brake` pred while stavkom.
	
	- Ce pogledas registre STM-a, imajo vsi __IO zraven.
	 To pomeni, da compiler NEBO rekel, eh TIMER value se nemore spremenit, ker se lahko s strani periferije,
	zato mora poskrbet, da jo vedno nalozi v register preden jo pregleda.
	
	Pameten compiler bi v tvojem primeru lahko celo vrgel ven (moral bi) while stavek in ga cist ignoriral.
	
	Just 4 future ;)
	*/

	return Fire_OK;
}


/**
  * @brief  This function counts trigger presses in programming mode
  * @note   The function returns immediatly if no press was detected, upon first press, it allows up to 2 seconds between presses
	* @note   If the trigger is held for more than 5 second, the motor will buzz and function will return 50 (presses)
	* @note   If the trigger is held for 5 more seconds, the motor will buzz again and the function will return 100 (presses)
  * @param  None
  * @retval Number of trigger presses
  */
uint8_t CountTriggerPresses(void)
{
	uint8_t n_press = 0;
	int timeout = 1; // start with timeout 1
	int held_timeout = 0;
	
	// wait for trigger
	while (timeout)
	{
		// if trigger pressed
		if (TriggerPressed())
		{
			// increase presses and allow some time between successive presses
			n_press++;
			timeout = TRIG_PRESS_TIMEOUT;
			
			// wait for trigger release
			while (TriggerPressed())
			{
				held_timeout++;
				
				if (held_timeout == TRIG_HELD_TIMEOUT1)
					MOTOR_LongBuzz(1);	
				
				if (held_timeout == TRIG_HELD_TIMEOUT2)
					MOTOR_LongBuzz(1);			

				if (held_timeout == TRIG_HELD_TIMEOUT3)
					MOTOR_LongBuzz(1);

				Delay_ms(10);
			}
			
			// check when the trigger was released
			if (held_timeout >= TRIG_HELD_TIMEOUT1 && held_timeout < TRIG_HELD_TIMEOUT2)
				return 30;
			if (held_timeout >= TRIG_HELD_TIMEOUT2 && held_timeout < TRIG_HELD_TIMEOUT3)
				return 60;
			else if (held_timeout >= TRIG_HELD_TIMEOUT3)
				return 100;
		}
		
		timeout--;
		Delay_ms(10);
	}
	
	return n_press;
}


// reset config data
void CONFIG_Reset(Config_t* Config)
{	
	Config->battery_type = BAT_LIPO_7_4;
	Config->fire_selector = SEL_SEMI_AUTO;
	Config->semi_time = 0;
	Config->burst_time = 0;
	Config->precocking = 0;
	Config->drive_pwm = 100;
	Config->brake_pwm = 100;
	Config->burst_to_auto = 400;
	Config->semi_delay = 0;
	
	Config->learned = 0;
	Config->learn_voltage = 0;
	
	Config->virgin_marker = 0;
}


// store config in flash
void CONFIG_Store(Config_t* Config)
{
	T_FLASH_Write(PAGE15_ADDR, (uint8_t*)Config, sizeof(*Config));
}


// read config from flash
void CONFIG_Read(Config_t* Config)
{
	T_FLASH_Read(PAGE15_ADDR, (uint8_t*)Config, sizeof(*Config));
	
	if (Config->virgin_marker > 0)
	{
		CONFIG_Reset(Config);
		CONFIG_Store(Config);
	}
}


// programming mode
void MENU_ProgrammingMode(Config_t* Config)
{
	uint8_t n, menu;
	
	// we're in programming mode
	while (1)
	{
		// count trigger presses
		n = CountTriggerPresses();
		
		// enter menu if pressed
		if (n > 0 && n <= 9)
		{
			// confirm menu selection
			MOTOR_Buzz(2);
			
			// selected menu is number of presses
			menu = n;
			
			// change variable in selected menu
			while (1)
			{
				n = CountTriggerPresses();
				
				// change variable if pressed
				if (n > 0)
				{
					// change variable
					switch (menu)
					{
						// battery select
						case 1:
							if (n <= 5)
								Config->battery_type = n;
							break;
						
						// fire selector layout
						case 2:
							if (n <= 4)
								Config->fire_selector = n;
							break;
						
						// ROF selection
						case 3:
							if (n == 1)
								Config->drive_pwm = 50;
							else if (n == 2)
								Config->drive_pwm = 60;
							else if (n == 3)
								Config->drive_pwm = 70;
							else if (n == 4)
								Config->drive_pwm = 80;
							else if (n == 5)
								Config->drive_pwm = 90;
							else if (n == 6)
								Config->drive_pwm = 100;
							break;
						
						// smaller cycle time
						case 4:
							if (Config->fire_selector == SEL_SEMI_SEMI)
							{
								Config->semi_time -= n * Config->semi_time/20; // 5% of semi cycle
								if (Config->semi_time < 0) // min value 0
									Config->semi_time = 0;
							}
							else if (Config->fire_selector == SEL_SEMI_BURST || Config->fire_selector == SEL_SEMI_BURST_H)
							{
								Config->burst_time -= n * Config->semi_time/20; // 5% of semi cycle
								if (Config->burst_time < Config->semi_time) // min value is semi value
									Config->burst_time = Config->semi_time;
							}
							break;
						
						// larger cycle time
						case 5:
							if (Config->fire_selector == SEL_SEMI_SEMI)
							{
								Config->semi_time += n * Config->semi_time/20; // 5% of semi cycle
								if (Config->semi_time > 20000) // 200 ms max
									Config->semi_time = 20000;
							}
							else if (Config->fire_selector == SEL_SEMI_BURST || Config->fire_selector == SEL_SEMI_BURST_H)
							{
								Config->burst_time += n * Config->semi_time/20; // 5 % of semi cycle
								if (Config->burst_time > 80000) // 800 ms max
									Config->burst_time = 80000;
							}
							break;
						
						// brake PWM selection
						case 6:
							if (n == 1)
								Config->brake_pwm = 0;
							else if (n == 2)
								Config->brake_pwm = 50;
							else if (n == 3)
								Config->brake_pwm = 80;
							else if (n == 4)
								Config->brake_pwm = 100;
							break;
						
						// precocking
						case 7:
							if (n == 1)
								Config->precocking = 0;
							else if (n == 2)
								Config->precocking = 40;
							else if (n == 3)
								Config->precocking = 50;
							else if (n == 4)
								Config->precocking = 60;
							else if (n == 5)
								Config->precocking = 70;
							else if (n == 6)
								Config->precocking = 80;
							else if (n == 7)
								Config->precocking = 90;
							break;
						
						// burst to auto time (in ms)
						case 8:
							if (n == 1)
								Config->burst_to_auto = 100;
							else if (n == 2)
								Config->burst_to_auto = 200;
							else if (n == 3)
								Config->burst_to_auto = 400;
							else if (n == 4)
								Config->burst_to_auto = 600;
							else if (n == 5)
								Config->burst_to_auto = 1000;
							break;
						
						// semi delay
						case 9:
							if (n == 1)
								Config->semi_delay = 0;
							else if (n == 2)
								Config->semi_delay = 200;
							else if (n == 3)
								Config->semi_delay = 500;
							else if (n == 4)
								Config->semi_delay = 1000;
							break;
						
						default: break;
					}

					// exit
					break;
				}
			}
			
			// store in flash
			CONFIG_Store(Config);
			
			// confirm operation done
			MOTOR_Buzz(3);
		}
		
		// handle longer trigger presses
		if (n == 30)
		{
			// exit menu
			return;
		}
		else if (n == 60)
		{
			// re-learn and exit
			Config->learned = 0;
			return;
		}
		else if (n == 100)
		{
			// reset all and exit
			CONFIG_Reset(Config);
			CONFIG_Store(Config);
			return;
		}
	}
}


// get battery level (from config data and voltage)
uint8_t GetBatteryLevel(Config_t* Config, int voltage)
{
	int low_value = 0, high_value = 0;
	int soc, diff;
	
	// set low and high values depending on battery
	switch (Config->battery_type)
	{
		case BAT_LIPO_7_4:
			low_value = 7000;
			high_value = 8400;
			break;
		case BAT_LIPO_11_1:
			low_value = 10500;
			high_value = 12600;
			break;
		case BAT_LIPO_14_8:
			low_value = 14000;
			high_value = 16800;
			break;
		case BAT_LIFE_9_6:
			low_value = 9000;
			high_value = 10800;
			break;
		case BAT_LIFE_12_8:
			low_value = 12000;
			high_value = 14400;
			break;
		default:
			break;
	}
	
	// calculate SoC
	diff = high_value - low_value;
	soc = 100 * (voltage - low_value) / diff;
	
	// limit soc
	if(soc > 100)
		soc = 100;
	else if(soc < 0)
		soc = 0;
	
	return (uint8_t)soc;
}


// is braking done?
uint8_t BrakingDone(void)
{
	if (brake == 0)
		return 1;
	else
		return 0;
}
