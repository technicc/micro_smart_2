/**	Tomaz Percic 2016										**/
/**	Library for uart functions					**/

#ifndef UART_H
#define UART_H

#include "stm32f0xx.h"
#include "buffer.h"

// defines

// USART1 PINPACK
	//USART1_PINPACK_1: PA2 TX, PA3 RX
	//USART1_PINPACK_2: PB6 TX, PB7 RX
	//USART1_PINPACK_3: PA9 TX, PA10 RX
#define USART1_PINPACK_4

// USART2 PINPACK
	//USART2_PINPACK1: PA2 TX, PA3 RX
	//USART2_PINPACK2: 
	//USART2_PINPACK3: 
#define USART2_PINPACK_2

// USART3 PINPACK
	//USART3_PINPACK1: PB10 TX, PB11 RX
	//USART3_PINPACK2: 
	//USART3_PINPACK3: 
#define USART3_PINPACK_1

// enable USART1 interrupt
//#define ENABLE_USART1_INT

// enable USART2 interrupt
//#define ENABLE_USART2_INT

// enable USART3 interrupt
//#define ENABLE_USART3_INT

// function prototypes
void T_USART_Init(USART_TypeDef* USARTx, int baud, buffer_struct_t* tmp_buffer);
void T_USART_SendChar(USART_TypeDef* USARTx, unsigned char character);
void T_USART_SendString(USART_TypeDef* USARTx, char* string);
int T_USART_GetChar(USART_TypeDef* USARTx, uint8_t* c);

int T_USART_ReadLine(USART_TypeDef* USARTx, char* buffer);


#endif
