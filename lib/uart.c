/**	Tomaz Percic 2016										**/
/**	Library for uart functions					**/

#include "uart.h"


// pointers to buffer structures
buffer_struct_t* USART1_RxBuffer;
buffer_struct_t* USART2_RxBuffer;
buffer_struct_t* USART3_RxBuffer;


// USART1 interrupt handlers
void USART1_IRQHandler(void)
{
	unsigned char rec_char;
	
	while(USART_GetITStatus(USART1, USART_IT_RXNE) == SET)
	{
		rec_char = USART_ReceiveData(USART1);
		Buffer_Write(USART1_RxBuffer, &rec_char, 1);
	}
}


// USART2 interrupt handlers
void USART2_IRQHandler(void)
{
	unsigned char rec_char;
	
	while(USART_GetITStatus(USART2, USART_IT_RXNE) == SET)
	{
		rec_char = USART_ReceiveData(USART2);
		Buffer_Write(USART2_RxBuffer, &rec_char, 1);
	}
}


// USART3 interrupt handlers
void USART3_6_IRQHandler(void)
{
	unsigned char rec_char;
	
	while(USART_GetITStatus(USART3, USART_IT_RXNE) == SET)
	{
		rec_char = USART_ReceiveData(USART3);
		Buffer_Write(USART3_RxBuffer, &rec_char, 1);
	}
}


// USART init
void T_USART_Init(USART_TypeDef* USARTx, int baud, buffer_struct_t* rx_buffer)
{	
	// structs
	GPIO_InitTypeDef GPIO_InitStruct;
	USART_InitTypeDef USART_InitStruct;
	
	// enable clocks USART1
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	
	// init pins
	#ifdef USART1_PINPACK_4
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource14, GPIO_AF_1);
	
		GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
		GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStruct.GPIO_Pin = GPIO_Pin_14;
		GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStruct.GPIO_Speed = GPIO_Speed_Level_3;
		GPIO_Init(GPIOA, &GPIO_InitStruct);
	#endif

	// init usart
	if (USARTx == USART1)
		RCC_USARTCLKConfig(RCC_USART1CLK_HSI);
	
	USART_InitStruct.USART_BaudRate = baud;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode = USART_Mode_Tx;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_Init(USARTx, &USART_InitStruct);
	USART_Cmd(USARTx, ENABLE);
}


// USART send character
void T_USART_SendChar(USART_TypeDef* USARTx, unsigned char character)
{
	while(USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET){}
	USART_SendData(USARTx, character);
}


// USART send string
void T_USART_SendString(USART_TypeDef* USARTx, char* string)
{
	while(*string)
	{
		while(USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET){}
			USART_SendData(USARTx, *string);
		string++;
	}
}


// USART get character
int T_USART_GetChar(USART_TypeDef* USARTx, uint8_t* c)
{
	if(USART_GetFlagStatus(USARTx, USART_FLAG_RXNE) != RESET)
	{
    *c = USART_ReceiveData(USARTx);	
		return 1;
	}
	else
		return 0;
}


// USART readline
int T_USART_ReadLine(USART_TypeDef* USARTx, char* buffer)
{
	uint8_t data, line_read;
	buffer_struct_t* RxBuffer;
	
	uint8_t *cnt;
	static uint8_t cnt1 = 0;
	static uint8_t cnt2 = 0;
	static uint8_t cnt3 = 0;
	
	// we didn't read a line yet
	line_read = 0;
	
	// select buffer which we will read from, select counter
	if (USARTx == USART1)
	{
		RxBuffer = USART1_RxBuffer;
		cnt = &cnt1;
	}
	else if (USARTx == USART2)
	{
		RxBuffer = USART2_RxBuffer;
		cnt = &cnt2;
	}
	else if (USARTx == USART3)
	{
		RxBuffer = USART3_RxBuffer;
		cnt = &cnt3;
	}
	
	// read single byte from buffer
	if (Buffer_Read(RxBuffer, &data, 1))
	{
		// read line
		if (data == '\r' || data == '\n')
		{
			buffer[*cnt] = '\0';
			*cnt = 0;
			line_read = 1;
		}
		else
		{
			buffer[(*cnt)++] = data;
			line_read = 0;
			
			if (*cnt > strlen(buffer))
				*cnt = 0;
		}
	}
	
	return line_read;
}
