/**	Tomaz Percic 2017										**/
/**	Library for board definitions				**/

#include "board_V02.h"

// inits board
void Board_Init(void)
{
	int div;
	
	// init structs
	GPIO_InitTypeDef GPIO_InitStruct;
	ADC_InitTypeDef ADC_InitStruct;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_OCInitTypeDef TIM_OCInitStruct;
	RCC_ClocksTypeDef RCC_Clocks;
	EXTI_InitTypeDef EXTI_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;

	// get MCU clock frequency
	RCC_GetClocksFreq(&RCC_Clocks);
	
	// calculate div for TIM3
	div = RCC_Clocks.PCLK_Frequency / (MOTOR_PWM_FREQ * MOTOR_PWM_LEVELS) - 1;
	if (div < 0)
		div = 0;
	
	// enable clocks
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	
	// init MSENSE pin
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Pin = MSENSE_PIN;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_Level_1;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	// LIN pin (not tied to TIM3)
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Pin = LIN_PIN;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_Level_1;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	// init HIN pin (tied to TIM3)
	GPIO_PinAFConfig(GPIOA, HIN_PINSRC, GPIO_AF_1);
	
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Pin = HIN_PIN;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_Level_1;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	// init TIM3 for HIN
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_Period = MOTOR_PWM_LEVELS;
	TIM_TimeBaseInitStruct.TIM_Prescaler = div;
	TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseInitStruct);
	
	TIM_OCStructInit(&TIM_OCInitStruct);
	TIM_OCInitStruct.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStruct.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStruct.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStruct.TIM_Pulse = 0;
	
	TIM_OC1Init(TIM3, &TIM_OCInitStruct);
	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);
	TIM_Cmd(TIM3, ENABLE);
	
	// init TRIG pin
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Pin = TRIG_PIN;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_Level_1;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	// init BATT pin
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Pin = BATT_PIN;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_Level_1;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	// ADC1 init (for MSENSE and TRIG)
	RCC_HSI14Cmd(ENABLE);
	while(RCC_GetFlagStatus(RCC_FLAG_HSI14RDY) != SET);
	ADC_ClockModeConfig(ADC1, ADC_ClockMode_AsynClk);
	RCC_HSI14ADCRequestCmd(ENABLE);
	
	ADC_GetCalibrationFactor(ADC1);
	
	ADC_InitStruct.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStruct.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStruct.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStruct.ADC_ScanDirection = ADC_ScanDirection_Upward;
	ADC_InitStruct.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStruct.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_TRGO;
	ADC_Init(ADC1, &ADC_InitStruct);
	ADC_Cmd(ADC1, ENABLE);
	
	ADC_AutoPowerOffCmd(ADC1, ENABLE);
	
	// init trigger exti
	EXTI_InitStruct.EXTI_Line = EXTI_Line10;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStruct);
	
	// configure RTC IRQ channels in NVIC
	NVIC_InitStruct.NVIC_IRQChannel = EXTI4_15_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);
	
	// init unused pins in analog pull-down
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_4 | GPIO_Pin_9;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_Level_1;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_Init(GPIOF, &GPIO_InitStruct);
	
	// unused USART pins
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_Level_1;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
}


// motor on
void MOTOR_On(void)
{	
	// turn on motor
	GPIOA->BSRR = LIN_PIN;
}

// motor off
void MOTOR_Off(void)
{
	// turn off motor
	GPIOA->BRR = LIN_PIN;
}


// set motor brake PWM
void MOTOR_SetBrakePWM(uint8_t pwm)
{	
	// set brake PWM
	if (pwm <= MOTOR_PWM_LEVELS)
		TIM3->CCR1 = pwm;
}


// get current flowing through motor
uint16_t MOTOR_GetCurrent(void)
{
	int current;
	
	ADC1->CHSELR = 0;
	ADC_ChannelConfig(ADC1, MSENSE_ADC_CHANNEL, MSENSE_ADC_SAMPLE_TIME);
	ADC_StartOfConversion(ADC1);
	
	while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) != SET);
	current = 3300 * ADC_GetConversionValue(ADC1) / 4096;
	
	return (uint16_t)current;
}


// get voltage from trigger
uint8_t TriggerPressed(void)
{
	if (GPIO_ReadInputDataBit(GPIOA, TRIG_PIN) == 1)
		return 1;
	else
		return 0;
}


// get voltage from battery
uint16_t GetBatteryVoltage(void)
{
	int voltage;
	
	ADC1->CHSELR = 0;
	ADC_ChannelConfig(ADC1, BATT_ADC_CHANNEL, BATT_ADC_SAMPLE_TIME);
	ADC_StartOfConversion(ADC1);
	
	while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) != SET);
	voltage = 17672 * ADC_GetConversionValue(ADC1) / 4096;
	
	return (uint16_t)voltage;
}


// buzz motor
void MOTOR_Buzz(int n_buzzes)
{
	int i, j;
	
	for (i=0; i<n_buzzes; i++)
	{
		for (j=0; j<10; j++)
		{
			MOTOR_On();
			Delay_us(500);
			MOTOR_Off();
			Delay_ms(20);
		}
		
		Delay_ms(300);
	}
}


// long buzz motor
void MOTOR_LongBuzz(int n_buzzes)
{
	int i, j;
	
	for (i=0; i<n_buzzes; i++)
	{
		for (j=0; j<35; j++)
		{
			MOTOR_On();
			Delay_us(500);
			MOTOR_Off();
			Delay_ms(20);
		}
		
		Delay_ms(300);
	}
}


// motor beep up
void MOTOR_BeepUp(void)
{
	int i;
								// C     G     C
	int notes[3] = {1811, 1175, 855};
	
	// play
	for (i=0; i<3; i++)
	{
		// one note
		TIM14->CNT = 0;
		while (TIM14->CNT < 15000)
		{
			MOTOR_On();
			Delay_us(100);
			MOTOR_Off();
			Delay_us(notes[i]);
		}
	}
}


void MOTOR_BeepDown(void)
{
	int i;
								// C     G     C
	int notes[3] = {855, 1175, 1811};
	
	// play
	for (i=0; i<3; i++)
	{
		// one note
		TIM14->CNT = 0;
		while (TIM14->CNT < 15000)
		{
			MOTOR_On();
			Delay_us(100);
			MOTOR_Off();
			Delay_us(notes[i]);
		}
	}
}


// motor play music
void MOTOR_Play(void)
{
	int i;
								// C     E     G     B     A     G     E     C    D      F     A     C    B
	int notes[13] = {1811, 1417, 1175, 912, 1036, 1175, 1417, 1811, 1602, 1331, 1036, 855, 912};
	
	// play
	for (i=0; i<13; i++)
	{
		// one note
		TIM14->CNT = 0;
		while (TIM14->CNT < 25000)
		{
			MOTOR_On();
			Delay_us(100);
			MOTOR_Off();
			Delay_us(notes[i]);
		}
	}	
}


// get chip temperature
float GetChipTemperature(void)
{
	uint16_t raw_temp;
	uint16_t temp_calib30;
	int temp;
	
	// enable temperature sensor
	ADC_TempSensorCmd(ENABLE);
	
	// wait for sensor startup
	Delay_us(15);
	
	// reset channels and select the correct one for temperature
	ADC1->CHSELR = 0;
	ADC_ChannelConfig(ADC1, ADC_Channel_16, ADC_SampleTime_55_5Cycles); // 4 us minimum sample time
	
	// get raw value
	ADC_StartOfConversion(ADC1);
	while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) != SET);
	raw_temp = ADC_GetConversionValue(ADC1);
	
	// get calibrated value at 30�C from flash
	temp_calib30 = *(uint16_t*)0x1FFFF7B8;
	
	// calculate temperature
	temp = (1873 * (temp_calib30 - raw_temp) / 10000) + 25;
	
	// slope = 4.3 mV/degree
	// inv_slope = 0.2326 degree/mV
	// slope = 5.34 ADCcount/degree
	// inv_slope = 0.1873 degree/ADCcount
	
	// disable temperature sensor
	ADC_TempSensorCmd(DISABLE);
	
	// return
	return temp;
}
