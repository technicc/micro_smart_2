/**	Tomaz Percic 2017										**/
/**	Library for RGB LED on mosfet				**/

#ifndef _RGB_LED_H
#define _RGB_LED_H

#include "stm32f0xx.h"
#include "rgb_led.h"
#include "board_V02.h"


// function prototypes
void LED_Driver(void);
void LED_SetRGB(uint8_t red_val, uint8_t green_val, uint8_t blue_val);
void LED_Intro(void);
void LED_Off(void);
void LED_DisplayValue(uint8_t percent);




#endif
