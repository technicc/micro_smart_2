/**	Tomaz Percic 2016										**/
/**	Library for buffer functions				**/

#include "buffer.h"

void Buffer_Init(buffer_struct_t *structure, int size)
{
	structure->read = 0;
	structure->write = 0;
	structure->size = size;
	structure->data = (uint8_t *)malloc(structure->size * sizeof(uint8_t));
}


void Buffer_Reset(buffer_struct_t *structure) 
{	
	structure->read = 0;
	structure->write = 0;
}


int Buffer_Write(buffer_struct_t *structure, uint8_t *data, int size) {
	int i;
	
	for(i=0; i<size; i++)
	{
		// check if buffer full
		if(structure->write == structure->read - 1 || (structure->read == 0 && (structure->write == structure->size))) 
			break;
		
		// else write in buffer
		structure->data[structure->write] = data[i];
		structure->write++;
		
		if(structure->write >= structure->size)
			structure->write = 0;	
	}
	return i;
}


int Buffer_Read(buffer_struct_t *structure, uint8_t *data, int size)
{
	int i = 0;
	
	if(Buffer_RemainingSpace(structure) < size) 
		return 0;
	
	while(structure->read != structure->write && i < size)
	{
		data[i] = structure->data[structure->read];
		
		structure->read++;
		if(structure->read >= structure->size)
			structure->read = 0;
		
		i++;
	}
	return i;
}


unsigned int Buffer_RemainingSpace(buffer_struct_t *structure)
{
	unsigned int size;
	unsigned int read = structure->read;
	unsigned int write = structure->write;
	
	if(write == read)
		size = 0;
	else if(write > read)
		size = write - read;
	else if(write < read)
		size = structure->size - (read - write);

	return size;
}
