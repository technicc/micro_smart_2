/*****************************************/ 
/**	Tomaz Percic 2015										**/
/**																			**/
/**	Library for user functions for the	**/
/**	F0xx chip														**/
/*****************************************/

#include "user_functions.h"

/**
  * @brief  This function sets the HSI and PLL to be used as system clock
  * @note   The function FLASH_SetLatency(FLASH_Latency_1); should be called before if sysclk will be above 24 MHz
  * @param  pll_mult: PLL multiplier from HSI/2 (4 MHz)
						This parameter can be RCC_PLLMul_x where x:[2,16] 
  * @retval None
  */
void Init_SetPLLHSIClock(uint32_t pll_mult)
{
	// enable HSI and disable HSE
	RCC_HSICmd(ENABLE);
	while(RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET);
	RCC_HSEConfig(RCC_HSE_OFF);
	
	// disable PLL
	RCC_PLLCmd(DISABLE);
	while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == SET);
	
	// set pll_multiplier
	RCC_PLLConfig(RCC_PLLSource_HSI_Div2, pll_mult);
	
	// enable PLL
	RCC_PLLCmd(ENABLE);
	while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET);
	
	// switch clock
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
}


// inits TIM14
void TIM14_Init(void)
{
	RCC_ClocksTypeDef RCC_Clocks;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	int div;
	
	// enable clock
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM14, ENABLE);
	
	// get MCU clock frequency
	RCC_GetClocksFreq(&RCC_Clocks);
	div = RCC_Clocks.PCLK_Frequency / 100000;
	
	// init TIM17
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_Period = 0xFFFF; //650 ms max
	TIM_TimeBaseInitStruct.TIM_Prescaler = div-1; //10 us step
	TIM_TimeBaseInit(TIM14, &TIM_TimeBaseInitStruct);
	TIM_Cmd(TIM14, ENABLE);
}


// inits TIM17 for pause functions
void Delay_Init(void)
{	
	RCC_ClocksTypeDef RCC_Clocks;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	int div;
	
	// enable clock
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM17, ENABLE);
	
	// get MCU clock frequency
	RCC_GetClocksFreq(&RCC_Clocks);
	div = RCC_Clocks.PCLK_Frequency / 1000000;
	
	// init TIM17
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_Period = 0xFFFF; //5 s max
	TIM_TimeBaseInitStruct.TIM_Prescaler = div-1; //1 us step
	TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM17, &TIM_TimeBaseInitStruct);
	TIM_Cmd(TIM17, ENABLE);
}


// delay ms function
void Delay_ms(int val)
{
	while(val--)
		Delay_us(1000);
}


// delay us function
void Delay_us(int val)
{
	TIM17->CNT = 0;
	while(TIM17->CNT < val);
}
