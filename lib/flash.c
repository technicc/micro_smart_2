/*****************************************/ 
/**	Tomaz Percic 2016										**/
/**																			**/
/**	Library for writing and reading			**/
/**	from/to flash												**/
/*****************************************/

#include "flash.h"


/**
  * @brief  Writes data to one page of FLASH
  * @note   The function can write from 1 to 1024 (2048) bytes of data (depending on processor used)
	
  * @param  page_address: The page address in program memory to be written
  * @note   this parameter has to be the start address of a page (multiple of 1024 or 2048)
	
	* @param  data: pointer to data which will be stored
	
	* @param  size: size of data to be stored
	
  * @retval error:	0 if flash store successful
										1 if error
  */
uint8_t T_FLASH_Write(uint32_t page_address, uint8_t* data, uint32_t size)
{
	uint8_t error = 0;
	uint16_t halfword_blocks;
	uint16_t byte_blocks;
	uint16_t padding;
	
	// set write address
	uint32_t write_address = page_address;
	
	// determine number of half_word blocks
	halfword_blocks = (size>>1);
	byte_blocks = size%2;
	
	// unlock flash
	FLASH_Unlock();
	
	// erase flash page, return if error
	if (FLASH_ErasePage(page_address) != FLASH_COMPLETE)
	{
		error = 1;
	}

	// write halfword blocks
	while (halfword_blocks--)
	{
		// write to flash, return if error
		if (FLASH_ProgramHalfWord(write_address, *(uint16_t*)data) != FLASH_COMPLETE)
		{
			error = 1;
		}
		
		// test written value, if it doesn't match, error
		if (*(uint16_t*)write_address != *(uint16_t*)data)
		{
			error = 1;
		}
		
		// increase address and data (2 because we're writing half-words)
		write_address += 2;
		data += 2;
	}
	
	// handle any remaining single bytes
	if(byte_blocks)
	{
		// prepare padding for single bytes
		padding = (uint16_t) *(uint8_t*)data;
		
		// write to flash, return if error
		if (FLASH_ProgramHalfWord(write_address, padding) != FLASH_COMPLETE)
		{
			error = 1;
		}
		
		// test written value, if it doesn't match, error
		if (*(uint16_t*)write_address != *(uint16_t*)data)
		{
			error = 1;
		}
	}

	// lock flash
	FLASH_Lock();
	
	return error;
}


/**
  * @brief  Erases a specified page in flash memory
	
  * @param  page_address: The page address in program memory to be erased
  * @note   this parameter has to be the start address of a page (multiple of 1024 or 2048)
	
  * @retval error:	0 if page erase successful
										1 if error
  */
uint8_t T_FLASH_ErasePage(uint32_t page_address)
{
	uint8_t error = 0;
	
	// unclock flash
	FLASH_Unlock();
		
	// erase page, error
	if (FLASH_ErasePage(page_address) != FLASH_COMPLETE)
	{
		error = 1;
	}
	
	// lock flash
	FLASH_Lock();
	
	// return error
	return error;
}


// flash read function
void T_FLASH_Read(uint32_t page_address, uint8_t* data, uint16_t size)
{
	memcpy(data, (uint8_t*)page_address, size);
}
